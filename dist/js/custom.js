$(function () {
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
  });
  // $('input[type="checkbox"]#checkAll, input[type="checkbox"].checkChange').iCheck({
  //   checkboxClass: 'icheckbox_flat-green',
  //   radioClass: 'iradio_flat-green'
  // });


  $("#demo5").paginate({
    count: 2,
    start: 1,
    display: 7,
    border: true,
    border_color: '#fff',
    text_color: '#fff',
    background_color: '#3C8DBC',
    border_hover_color: '#ccc',
    text_hover_color: '#fff',
    background_hover_color: '#36A978',
    images: false,
    mouse: 'press',
    onChange: function(page){
      $('._current','#paginationdemo').removeClass('_current').hide();
      $('#p'+page).addClass('_current').show();
      }
  });
});

function getFile(){
  document.getElementById("upfile").click();
}

function getFileCS(){
  document.getElementById("upfileCS").click();
}
$('#upfileCS').change(function(){
  var filePath=$(this).val();
  // alert(filePath);
  $('.rtpProcess').show();
  $('.tableOverlay').show();
  setTimeout(function(){ $('#rtpPop').modal('show'); }, 4000);
});

$('.variationsCheck').change(function(event){
  if($(this).val() == 'Yes'){
    $('.variCheckRes').hide();
    $('.variCheckResYes').fadeIn();
  }
  if($(this).val() == 'No'){
    $('.variCheckRes').hide();
    $('.variCheckResNo').fadeIn();
  }
});

var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : {allow_single_deselect:true},
  '.chosen-select-no-single' : {disable_search_threshold:10},
  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
  '.chosen-select-width'     : {width:"95%"}
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}

$(document).ready(function() {
  $('.subTables').hide();
  $('.open').on('click', function() {
    $(this).parent('div').parent('div').find('.subTables').slideToggle();
    //$(this).find('.glyphicon-chevron-down').toggleClass('glyphicon-chevron-up');
    if ($(this).find('i').hasClass('glyphicon-chevron-down')) {
      $(this).find('i').removeClass('glyphicon-chevron-down');
      $(this).find('i').addClass('glyphicon-chevron-up');
    } else {
      $(this).find('i').removeClass('glyphicon-chevron-up');
      $(this).find('i').addClass('glyphicon-chevron-down');
    }
  });

  $('.becomeSeller').click(function(){
    $(this).hide();
    $(this).parents('.actionB').find('.approvalPendingA').fadeIn();
  });

  $("#checkAll").change(function(){
    var status = $(this).is(":checked") ? true : false;
    $(".checkChange").prop("checked",status);
  });
  // var selAll = $('#checkAll').parent('div');
  // //alert(selAll)
  // $('.selAll').children('.icheckbox_flat-green').click(function(){
  //   alert(1);
  // });
  // if($('.selAll > .icheckbox_flat-green').hasClass('checked')){
  //   alert(2)
  // };

  $("#upfile").change(function() {
    //alert('changed!');
    $("#updateImgFrm").submit();
  });
  $('#profile').addClass('current');

  $('.cnrfmVari').click(function(){
    $('#preVari').hide();
    $('#postVari').show();
    $('#variCount').html('3');
  });
  $('.createOutward').click(function(){
    $('.rtpProcess').show();
    $('.tableOverlay').show();
    setTimeout(function(){ $('#rtpPop').modal('show'); }, 4000);
  });
  $('.dwnldLater, #rtpPop .close').click(function(){
    $('#rtpPop').modal('hide');
    $('.rtpProcess').hide();
    $('.tableOverlay').hide();
    // window.location.href = 'ready-to-dispatch.html';
  });
  // $('#rtpPop').modal('show');
});
// document.ready ends here
$(function(){
  //Date range picker
  $('.reservation').daterangepicker();
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  CKEDITOR.replace('editor1');
  CKEDITOR.replace('editor2');
  CKEDITOR.replace('editor3');
  CKEDITOR.replace('editor4');
  CKEDITOR.replace('editor5');
  //bootstrap WYSIHTML5 - text editor
  // $(".textarea").wysihtml5();
})
//Products Inventory page checkbox functions
$('.checkChange').change(function(){
  var chkArr = 0;
  $('.checkChange:checked').each(function() {
      chkArr = chkArr + 1;
  });
  // alert(chkArr)
  if(chkArr >= 1) {
    $('.invActBox').show();
    $('#selectedProds').html(chkArr);
  } else {
    $('.invActBox').hide();
  }
});
$("#checkAllInv").change(function(){
  var status = $(this).is(":checked") ? true : false;
  var numItems = $('.checkChange').length;
  if(status == true) {
    $(".checkChange").prop("checked",status);
    $('.invActBox').show();
    $('#selectedProds').html(numItems);
  } else {
    $('.invActBox').hide();
    $(".checkChange").prop("checked",status);
  }
});
